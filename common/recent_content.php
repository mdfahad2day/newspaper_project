<div class="recent_container">
        <div class="recent_row">
            <div class="para_1">
                <h6>No more violence in the name of religion</h6>
                <p style="margin-top: 5px;">I grew up in a mixed family—my father was a practising Hindu and my mother was a practising Muslim. Even today, if somebody asks me about my religion, I fail to identify as just one of them; I am both, and definitely not one over the other. I grew up in Dhaka city at a time (1980-1990) when democracy was more under threat than religion. So, during my childhood years, I experienced a beautiful confluence of two faiths. My mother's family loved their Hindu jamai (son-in-law), and my father's family loved their Muslim bou (daughter-in-law). It was never an issue in our household whether we were celebrating Eid or Puja. We had the blessings of celebrating and enjoying both, without anyone raising any complaint about our celebrations.</p>
                <p style="margin-top: 5px;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum labore modi reiciendis in aliquid accusamus explicabo delectus aut nisi recusandae.</p>
            </div>
            <div class="recent_img_cls">
                <img style="width: 100%;" src="img/violence-religion.jpg" alt="">
            </div>
        </div>
    </div>