

<div class="wrapper">
        <div class="body_container">
            <div class="left_container">
                <ul>
                    <li><a href="javascript:void(0)" class="tablink" onclick="news_paper('ONE')">The Daily Star<a></li>
                    <li><a href="javascript:void(0)" class="tablink" onclick="news_paper('TWO')">The Daily Sun</a></li>
                    <li><a href="javascript:void(0)" class="tablink" onclick="news_paper('THREE')">The Daily Observer</a></li>
                </ul>
            </div>
            <div id="ONE" class="tabcontent">
                <div id="first_news" class="para">
                    <div class="header_1">
                        <h4>Dune: A stunning, ambitious adaptation for sci-fi fans</h4>
                    </div>
                    <div class="paragraph_1">
                        <p>Greig Frasier's breathtaking cinematography will have you gawking at something or someone throughout the course of the film. The thundering orchestra by Hans Zimmer sets the ominous mood of the film perfectly. The costume design by Jacqueline West and Robert Morgan are every bit as opulent and elaborate as it is peculiar.</p>
                    </div>
                </div>
            </div>

            <!-- The Daily Sun Content Started  -->
            <div id="TWO" class="tabcontent">
                <div id="first_news" class="para">
                    <div class="header_1">
                        <h4>6 Covid-19 deaths recorded in 24 hours, positivity rate now 1.5%</h4>
                    </div>
                    <div class="paragraph_1">
                        <p>A total of 19,535 samples were tested across the country in 24 hours (till 8am today). At least 227 Covid-19 patients have recovered in this period. The total number of recoveries now stands at 15,32,695 and the recovery rate at 97.7 percent.</p>
                        <p>Among the six deceased, three were men and three women. Of them one was within 31-40 years old; one was within 51-60; two were between 61-70; and two were within 71-80 years old, added the release.</p>
                    </div>
                </div>
            </div>

            <div id="THREE" class="tabcontent">
                <div id="first_news" class="para">
                    <div class="header_3">
                        <h4>‘Bangladesh Art Week’ hosts three events in Dubai </h4>
                    </div>
                    <div class="paragraph_1">
                        <p>I feel honoured to be able to represent Bangladesh internationally," says Niharika Momtaz, Founder, BAW. "This is a big step towards setting the international stage for our artists."</p>
                        <p>The exhibitions showcase a variety of sculptures, photographs, mixed media constructions, site-specific installations, works on paper, and film presentations.</p>
                    </div>
                </div>
            </div>
    </div>

    <!-- body_news_ending -->
    <div class="post">
        <div class="post_row">
            <div class="book_post">
                <div class="thumb_img">
                    <img src="img/img.jpg" alt="Evening Image">
                </div>
                <div class="post">
                    <div class="header">
                        <h4>Header 1</h4>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex atque dolorem itaque perspiciatis iure, dicta nisi? Minus in voluptatibus unde!</p>
                    </div>
                </div>
            </div>

            <div class="book_post">
                <div class="thumb_img">
                    <img src="img/romantic.jpg" alt="Evening Image">
                </div>
                <div class="post">
                    <div class="header">
                        <h4>Header 2</h4>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex atque dolorem itaque perspiciatis iure, dicta nisi? Minus in voluptatibus unde!</p>
                    </div>
                </div>
            </div>

            <div class="book_post">
                <div class="thumb_img">
                    <img src="img/love.jpg" alt="Evening Image">
                </div>
                <div class="post">
                    <div class="header">
                        <h4>Header 3</h4>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex atque dolorem itaque perspiciatis iure, dicta nisi? Minus in voluptatibus unde!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div>


<?php


  



?>