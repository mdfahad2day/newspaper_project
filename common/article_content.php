<!-- Article Start -->
<div class="flex-container">
        <div class="flex-row" style="margin-left: 5px;">
            <div class="flex_image">
                <img src="img/article_1.jpg" alt="">
            </div>
            <div class="flex_header">
                <h3>How to Sell Books</h3>
            </div>
            <div class="flex_body">
                <p>We all know that books are fundamental tools for learning and entertainment, at least, very much so in the recent past, before the advent of digital equivalents, such as eBooks. The circumstances may be evolving little by little. But that doesn’t necessarily mean books are just mere relics.</p>
            </div>
        </div>
        <!-- flex-row two -->
        <div class="flex-row" style="margin-left: 20px;">
            <div class="flex_image">
                <img src="img/article_2.jpg" alt="">
            </div>
            <div class="flex_header">
                <h3>How to Prepare for a Quick and Smooth Move</h3>
            </div>
            <div class="flex_body">
                <p>We all know that books are fundamental tools for learning and entertainment, at least, very much so in the recent past, before the advent of digital equivalents, such as eBooks. The circumstances may be evolving little by little. But that doesn’t necessarily mean books are just mere relics.</p>
            </div>
        </div>
        <!-- flex-row-three -->
        <div class="flex-row" style="margin-left: 20px;">
            <div class="flex_image">
                <img src="img/article_3.jpg" alt="">
            </div>
            <div class="flex_header">
                <h3>How to Sell Pokémon Cards for the Most Money</h3>
            </div>
            <div class="flex_body">
                <p>We all know that books are fundamental tools for learning and entertainment, at least, very much so in the recent past, before the advent of digital equivalents, such as eBooks. The circumstances may be evolving little by little. But that doesn’t necessarily mean books are just mere relics.</p>
            </div>
        </div>
    </div>