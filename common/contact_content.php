 <!-- contact page form started -->
 <div class="contact_container">
        <div class="container_left">
            <form action="">
                <div class="row">
                    <label class="label_clss" for="">Full Name : </label>
                    <input class="input_clss" type="text" name="full_name" id="">
                </div>
                <div class="row">
                    <label class="label_clss" for="">Subject : </label>
                    <input class="input_clss" type="text" name="subject" id="">
                </div>
                <div class="row">
                    <label class="label_clss" for="">Email : </label>
                    <input class="input_clss" type="email" name="email" id="">
                </div>
                <div class="row">
                    <label class="label_clss" for="">Message : </label>
                    <textarea class="input_clss" type="textarea" name="message" id="" cols="30" rows="10%" placeholder="Enter Your Message . . ."></textarea>
                </div>
                <button class="submit_btn" type="submit">Submit</button>    
            </form>
            
        </div>
    </div>

    <div class="container_right">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.798707675446!2d90.3697051143892!3d23.79018129317372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c15097199e5d%3A0xe5692c9214544ab3!2sPigeon%20IT%20Limited!5e0!3m2!1sen!2sbd!4v1635236788326!5m2!1sen!2sbd" width="600" height="420" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>